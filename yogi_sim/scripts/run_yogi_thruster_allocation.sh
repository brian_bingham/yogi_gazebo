#!/bin/bash

: "${DEVWD?Need to set DEVWD - E.G. export DEVWD=~/development} or source set-cwd-as-devwd-1"

thrust-allocation-lcm -d -t yogi_thrusters_t --tx-channel VELOCITY_CMD --rx-channel EFFORT_CMD --ctrl-mode-channel CONTROL_MODE_CMD --configfilename ${DEVWD}/yogi_configs/tal.yaml
