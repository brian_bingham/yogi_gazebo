#!/bin/bash

echo "Run xacro to generate URDF"
rosrun xacro xacro --inorder yogi.xacro > yogi.urdf

echo "Use gazebo to generate SDF"
gz sdf --print yogi.urdf > yogi.sdf

